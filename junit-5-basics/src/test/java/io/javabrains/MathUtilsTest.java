package io.javabrains;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestReporter;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

@DisplayName("When running MathUtils.")
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class MathUtilsTest {

	MathUtils mathUtils; 
	TestInfo testInfo; 
	TestReporter testReporter;
	
	@BeforeAll
	static void beforeAllInit() {
		System.out.println("This needs to run before all.");
	}
	
	@BeforeEach
	void init(TestInfo testInfo, TestReporter testReporter) {
		this.testInfo = testInfo; 
		this.testReporter = testReporter;
		mathUtils = new MathUtils(); 
		testReporter.publishEntry("Running " + testInfo.getDisplayName()+ " with tags " + testInfo.getTags());
	}
	
	@AfterEach
	void cleanUp () {
		System.out.println("Cleaning up.");
	}
	
	@Test
	@DisplayName("Testing add method.")
	void testAdd() {		
		int expected = 2; 
		int actual = mathUtils.add(1, 1);		
		assertEquals(expected, actual, "The add method should add two numbers.");
	}
	
//  1. easy to organize. Grouping. 
//	2. It affects the failure rate of these tests. If any of the test fails then my add method is not working. 
//  3. The success or failure of AddTest is the cumulative of all the children. 
//	4. This AddTest would pass when all the children would pass. 

	
	@Nested
	@DisplayName("add method")
	@Tag("Math")
	class AddTest {
		
		@Test
		@DisplayName("when adding two positive numbers")
		void testAddPositive() {		
//			int expected = 2; 
//			int actual = mathUtils.add(1, 1);		
			assertEquals(2, mathUtils.add(1, 1), "should return the right sum");
		}
		
		
		@Test
		@DisplayName("when adding two positive numbers")
		void testAddNegative() {		
			int expected = -2; 
			int actual = mathUtils.add(-1, -1);		
			assertEquals(expected, actual, () -> "should return sum" + expected + "but returned " + actual);
											// execute the method only if the test fails. 
			 								// if the test passes then I am not going to execute the method. 
											// computation is not going to happen until and unless the test fails. 
		}
		
	}
	
	
	@Test
	@DisplayName("multiply method.")
	@Tag("Math")
	void testMultiply() {
//		System.out.println("Running " + testInfo.getDisplayName()+ " with tags " + testInfo.getTags());
//		testReporter.publishEntry("Running " + testInfo.getDisplayName()+ " with tags " + testInfo.getTags());
//		assertEquals(4, mathUtils.multiply(2, 2), "should return the right product.");
		assertAll(
				() -> assertEquals(4, mathUtils.multiply(2, 2), "should return the right product."),
				() -> assertEquals(0, mathUtils.multiply(2, 0), "should return the right product."),
				() -> assertEquals(-2, mathUtils.multiply(2, -1), "should return the right product.")				
				);
	}
		
	
	@Test
	@DisplayName("divide method.")
	@EnabledOnOs(OS.MAC)
	@Tag("Math")
	void testDivide() {		
		boolean isServerUp = false;		
		assumeTrue(isServerUp);		// if the server is up then execute this test. 
		
		assertThrows(ArithmeticException.class, () -> mathUtils.divide(1, 0), "Divide by zero should throw.");
//		assertThrows(NullPointerException.class, () -> mathUtils.divide(1, 0), "Divide by zero should throw.");
		
//		Executable is a functional interface that can be used to implement any generic block of code that potentially throws a Throwable.
//		The Executable interface is similar to Runnable, except that an Executable can throw any kind of exception.		
//		Test would fail for two reasons: 
//		1. It does not throw an exception. 
//		2. A wrong exception is thrown. 
		
	}
	
	@Tag("Circle")
	@RepeatedTest(3)
	@DisplayName("Testing circle area method.")
	void testComputeCircleArea(RepetitionInfo repitionInfo) {			
		assertEquals(314.1592653589793, mathUtils.computeCircleArea(10), "Should return right circle area.");
	}
	
	@Test
	@Disabled
	@DisplayName("TDD method. Should not run.")
	void disabled() {
		fail("This test should be disabled.");	
	}
	
}
